---
aliases: ["Tokenomics Roadmap", "TPBL Public"]
roadmap: true
public: true
updated: 2022-11-28
---

Weekly Meeting: 1530 UTC on 2022-11-29

Current Poll: Can we officially change the start time to 1500 UTC, effective Tuesday 2022-12-06?

[Link to Current Miro Board](https://miro.com/app/board/uXjVPO7Pofs=/)

---

## Roadmap

<div class="roadmapTable">
	<table>
		<tr>
			<th rowspan="2">December 2022</th>
			<td class="project">Multi-Sig</td>
			<td>
				<ul>
				<li>Multi-sig wallet will hold all Gimbal tokens in Allocations 1 through 7.</li>
				<li>Define list of people</li>
				<li>Define rules</li>
				<li>Define technical process for collecting signatures</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Decision Processes</td>
			<td>
				<ul>
				<li>When will we revisit the selection of gimbal key holders?</li>
				<li>Are there technical experiments we want to try?</li>
				<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">January 2023</th>
			<td class="project">TPBL Book Club</td>
			<td>
				<ul>
					  <li>Reading <a href="https://www.akpress.org/emergentstrategy.html">Emergent Strategy</a> by adrienne maree brown</li>
					  <li>First book club meeting 2023-01-03</li>
					  <li>Guiding Question: After reading this book, what do you think needs to be considered in the design of the gimbal token? How will that be reflected in a TPBL Course?</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Prepare for TPBL Launch</td>
			<td>
				<ul>
				<li>When will we revisit the selection of gimbal key holders?</li>
				<li>Are there technical experiments we want to try?</li>
				<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">February-March 2023</th>
			<td class="project">Launch Tokenomics PBL</td>
			<td>
				<ul>
					<li>Define projects</li>
					<li>Create course modules</li>
					<li>Meeting schedule</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Develop Gimbal Tokenomics</td>
			<td>
				<ul>
				<li>With community</li>
				<li>Comprehensive plan for what this means</li>
				</ul>
			</td>
		</tr>
	</table>
</div>

