---
aliases: ["PPBL Roadmap", "PPBL Public"]
roadmap: true
public: true
updated: 2022-11-30
---

### Goals
1. Plutus PBL effectively onboards new developers to Cardano development
2. Plutus PBL enables shared ownership of projects and decision-making at Gimbalabs
3. Plutus PBL creates space where developers can trust and rely on each other

## Guiding Questions:
- Now that we have Demeter.run and Mesh project templates, how should we change the 100- and 200-level modules in the PPBL Course?
- How can we keep the course up to date so Onboarding and BBK are current?
- How can we extend the course so that advanced topics are covered?

## Key Tasks
- Keeping repositories up to date. 
- Keeping course modules up to date.
- Writing PPBL Project descriptions on GPTE board. See [[gpte-roadmap|GPTE Roadmap]]

# Roadmap
<div class="roadmapTable">
	<table>
		<tr>
			<th rowspan="2">December 2022</th>
			<td class="project">Develop a plan for ongoing revisions to existing course modules</td>
			<td>
				<ul>
					<li>Shared responsibilities: course maintenance</li>
					<li>Shared responsibilities: adding new modules</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Develop roadmap/milestones for adding new course modules</td>
			<td>
				<ul>
					<li>What tools should we highlight?</li>
					<li>What problems need more attention?</li>
					<li>For example, a CBOR parser?</li>
					<li>For example, transaction batching?</li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="3">January 2023</th>
			<td class="project">Ongoing support + Live Coding</td>
			<td>
				<ul>
					<li>How can we best support developers to onboard to PPBL?</li>
					<li>Is the current Live Coding schedule sufficient for our needs?</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Outreach to additional communities</td>
			<td>
				<ul>
					<li>Global Gimbalabs communities</li>
					<li>Offering PBL as a service</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Ongoing course development</td>
			<td>
				<ul>
					<li>Revisions</li>
					<li>New Modules</li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">February-March 2023</th>
			<td class="project">When to launch next cohort of course?</td>
			<td>
				<ul>
					<li>What is the decision criteria for launching "V3" of PPBL Course?</li>
					<li>Distinguishing between ongoing course vs. cohorts of learners</li>
				</ul>
			</td>
		</tr>
	</table>
</div>
