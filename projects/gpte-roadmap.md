---
aliases: ["GPTE Roadmap", "GPTE Public"]
roadmap: true
public: true
updated: 2022-11-30
---

# Gimbal Project Treasury + Escrow (GPTE)
- Live on Pre-Production: https://gpte.gimbalabs.io/
- Code repositories on GitLab: https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte

## Goals of GPTE
1. Organize work of project teams
2. Build reputation of Contributors
3. Establish a real use-case for a full-stack, headless dapp on Cardano

## Guiding Questions

### How can we make the GPTE stack accessible to devs / teams?

**By providing documentation for:**
- Plutus Contracts: https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte/gpte-plutus-v2
- CBOR Hex Automation: https://github.com/SIDANWhatever/plutus-cborhex-automation
- Front End: https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte/gpte-front-end
- Course Completion tokens with reference NFTs that can updated: https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/completion-token/ppbl-contributor-token

**By running Plutus PBL Course**
- Keeping the course up to date
- Continuing to support newcomers to course
- See [[ppbl-roadmap]]

**By distributing ownership of:**
1. GPTE Project descriptions
2. Approval processes
3. Gimbalabs Treasury

### How can we onboard developers to Contributing at Gimbalabs?
1. How can we best onboard seasoned developers to getting a Contributor Token? For example, if they don't need/want to review the entire PPBL Course?
2. Shared ownership of ongoing development: from `Project` descriptions (in initial GPTE Mainnet) to creating new instances of GPTE. 
3. Accessible apis and "ease of use" while minimizing single points of failure
4. Establishing use case for Gimbal Token. See: [[tokenomics-roadmap]]


# Roadmap:

<div class="roadmapTable">
	<table>
		<tr>
			<th rowspan="2">December 2022</th>
			<td class="project">Initial Mainnet Release of GPTE</td>
			<td>
				<ul>
					<li>Refine the current list of Project descriptions</li>
					<li>Finalize plans for PPBL Completion Token</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Distribute v1 of PPBL Completion Token</td>
			<td>
				<ul>
					<li>Metadatum and update process</li>
					<li>CIP-25 compliant visual/artwork</li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">January 2023</th>
			<td class="project">Developing GPTE</td>
			<td>
				<ul>
					<li>By committing to and completing listed Projects</li>
					<li>By adding/revising existing Project Descriptions</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Shared ownership of Project Descriptions</td>
			<td>
				<ul>
					<li>Can we dedicate some PPBL Live Coding time to writing Project Descriptions</li>
					<li>This is a form of governance</li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">February-March 2023</th>
			<td class="project">Releasing well-documented components</td>
			<td>
				<ul>
					<li>See GPTE Components section below</li>
				</ul>
			</td>
		</tr>
	</table>
</div>


---

# Current State:

There are currently two version of Gimbal Project Treasury and Escrow (GPTE). One is built on PlutusV1 and is shared in the current version of the [PPBL Course in Canvas](). The other is built on PlutusV2 and is the current official version of GPTE.

## PlutusV1
Updated 2022-11-04: https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-course-02/-/tree/master/project-303
How should we support the Pre-Prod deployment?
What are the right next steps?

#### Current Front End: Talk to team about what we should share

#### In course: Module 303 - experiment with Trevor owning it
GBTE in Project 303 - Got it working on Plutus
- [ ] Updated Docs are [live on GitLab](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-course-02/-/tree/master/project-303/bounty-treasury-escrow)
- [ ] Plan for Canvas Course

## PlutusV2
Updated 2022-11-04: **https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte/gpte-plutus-v2

#### Current Front End (GPTE)
**https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte/gpte-front-end
- [ ] Of course the PPP Option: Use NFT to prove that this UTxO has the correct Datum
- [ ] Mesh upgrades - implement hooks - on new branch? (2022-11-04 action!)
- [ ] How can we ensure that a given Bounty can be Committed to only once? (Bounty 0003)
- [ ] Every Thursday at Live Coding: Review Project Descriptions

I feel pretty good about it! There are two things it could be:
1. Less likely? But worth asking - The way I'm resolving Datum is not quite right
2. More likely? I'm still just waiting on the CSL fix....could this be? In any case, save that branch.

#### In course: Not Yet

#### Should we start to build GPTE Back End?
- ie for Commenting on Bounties
- can create an intersting Hashing process, chain of hashes that reflect changes

#### Potentially Explore:
- [ ] Use of Ref TxIns and Datum: https://github.com/james-iohk/plutus-scripts/blob/master/src/CheckSameInlineDatumAtAllInputs.hs -- I think I have some ideas for how to use this.


---

# GPTE Components

## Plutus

## Front End

## PPBL Contributor Token

## Coming Soon: Approval Mechanism

## Coming Soon: Self-Service Minter