## Use Live Coding videos
- [ ] Video currenly holds weekly updates - what if we replace that with Live Coding?
- [ ] What if we replace PBL or CSK with Live Coding?
- [ ] Is this a chance to invite new contribution?

## Update /mastery-policy
- [ ] Minimum: add link to GPTE
- [ ] What other messaging
- [ ] Of course this connects to [[PPBL Completion Token]]

- What do we need to communicate?
- Who is on the team?


<div class="roadmapTable">
	<table>
		<tr>
			<th rowspan="2">December 2022</th>
			<td class="project">Build Team</td>
			<td>
				<ul>
					<li></li>
					<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Create initial list of goals / outcomes</td>
			<td>
				<ul>
					<li></li>
					<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">January 2023</th>
			<td class="project">Shared maintenance</td>
			<td>
				<ul>
					<li></li>
					<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Team Page</td>
			<td>
				<ul>
					<li></li>
					<li></li>
				</ul>
			</td>
		</tr>
	</table>
</div>
