- How can we follow the model of PPBL and TPBL and just get started?
- Does this align with Dev Ops?


<div class="roadmapTable">
	<table>
		<tr>
			<th rowspan="2">December 2022</th>
			<td class="project">Planning</td>
			<td>
				<ul>
					<li>Course outline</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Building Team</td>
			<td>
				<ul>
					<li>Can this be combined with some kind of DevOps PBL?</li>
					<li></li>
					<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">January 2023</th>
			<td class="project">Initial Experiment</td>
			<td>
				<ul>
					<li>Start small: Look at how PPBL and TPBL started</li>
					<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project"></td>
			<td>
				<ul>
					<li>Focus of Live Coding every Thursday?</li>
					<li></li>
				</ul>
			</td>
		</tr>
		<tr>
			<th rowspan="2">February-March 2023</th>
			<td class="project">Launch first cohort</td>
			<td>
				<ul>
					<li>Outline</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td class="project">Sharing + Supporting</td>
			<td>
				<ul>
					<li></li>
					<li></li>
				</ul>
			</td>
		</tr>
	</table>
</div>
