---
aliases: ["Gimbalabs Roadmap", "Roadmap Public"]
roadmap: true
public: true
updated: 2022-11-30
---




---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/playground-roadmap">
			Gimbalabs Playground
		</a>
	</h1>
	<div class="why">
		<p>
			Gimbalabs hosts a weekly Playground meeting where developers from the Cardano community can present their work, and where newcomers to the Cardano community can present their ideas. Anyone is invited to join or to sign up to be a presenter.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Meet every Tuedsay at 1800 UTC</li>
		</ul>
	</div>
</div>


---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/dandelion-roadmap">
			Dandelion
		</a>
	</h1>
	<div class="why">
		<p>
			Gimbalabs is building Dandelion because we expect the Cardano API layerto be as resilient as the Cardano Protocol.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Organize a group to create Dandelion PBL.</li>
		</ul>
	</div>
</div>

---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/dandelion-pbl-roadmap">
			Dandelion PBL
		</a>
	</h1>
	<div class="why">
		<p>
			Gimbalabs is building Dandelion PBL (DPBL) because we want people to be able to deploy Dandelion nodes, which builds the resiliency of the Dandelion network.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Create a draft for V1 of DPBL Course</li>
		</ul>
	</div>
</div>

---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/tokenomics-roadmap">
			Gimbal Tokenomics
		</a>
	</h1>
	<div class="why">
		<p>
			Gimbalabs is developing the Gimbal token because we think native assets can be used to restore balance in the lives of people who build things.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Initialize multi-sig wallet for Allocations 1 through 7</li>
		</ul>
	</div>
</div>

---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/ppbl-roadmap">
			Plutus PBL
		</a>
	</h1>
	<div class="why">
		<p>
			Gimbalabs provides the Plutus Project-Based Learning as a free service because we want to help people start building on Cardano, and because people can develop trust when they learn together. Our community has a proven track record of launching new collaborations.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Now that Demeter.run and Mesh templates exist, how can we adjust the Plutus PBL Course?</li>
		</ul>
	</div>
</div>

---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/gpte-roadmap">
			Gimbal Project Treasury and Escrow
		</a>
	</h1>
	<div class="why">
		<p>
			Gimbalabs is building the GPTE dapp so that anyone can organize work locally while building reputation among a global community of developers.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Launch GPTE V1 on Mainnet</li>
		</ul>
	</div>
</div>

---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/gimbalabs-web-site-roadmap">
			Partner Projects
		</a>
	</h1>
	<div class="why">
		<p>
			Gimbalabs is building ways of working together. Our current partner projects allow us to create trusted teams, build our track record, and inform the work of Gimbal Tokenomics.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Launch development work on AstraNFT</li>
		</ul>
	</div>
</div>

---
<div>
	<h1 class="project-overview">
		<a href="/gimbalabs/401-gimbalabs/gimbalabs-roadmap/projects/gimbalabs-web-site-roadmap">
			gimbalabs.com
		</a>
	</h1>
	<div class="why">
		<p>
			The Gimbalabs web site should celebrate community and minimize friction. We must improve communications and help people find out what is happening at gimbalabs.
		</p>
	</div>
	<div class="next-step">
		<h3>Next Step</h3>
		<ul>
			<li>Update live calendar and add calls to action on homepage</li>
		</ul>
	</div>
</div>

---
# Impact


---
# Testimonials
